import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:technight_tmdb/src/movies/movies_screen.dart';
import 'package:technight_tmdb/src/tmdb/tmdb_service.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<TmdbService>(
      create: (context) => TmdbService(),
      child: MaterialApp(
        theme: ThemeData.dark(),
        home: MoviesScreen(),
      ),
    );
  }
}
