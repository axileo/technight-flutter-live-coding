import 'package:flutter/foundation.dart';

class TmdbMovie {
  final int id;
  final String title;
  final String overview;
  final String posterPath;
  final String backdropPath;

  TmdbMovie({
    @required this.id,
    @required this.title,
    @required this.overview,
    @required this.posterPath,
    @required this.backdropPath,
  });
}
