import 'package:dio/dio.dart';
import 'package:technight_tmdb/src/tmdb/tmdb_api_key.dart';
import 'package:technight_tmdb/src/tmdb/tmdb_movie.dart';

class TmdbService {
  final client = Dio(
    BaseOptions(
      baseUrl: "https://api.themoviedb.org/3",
      queryParameters: {
        "api_key": tmdbApiKey,
      },
    ),
  );

  Future<List<TmdbMovie>> getMovies() async {
    final response = await client.get("/movie/popular");
    final data = response.data;
    final movies = data["results"] as List;
    return movies.map((movie) {
      return TmdbMovie(
        id: movie["id"] as int,
        title: movie["title"] as String,
        overview: movie["overview"] as String,
        posterPath: movie["poster_path"] as String,
        backdropPath: movie["backdrop_path"] as String,
      );
    }).toList();
  }
}
