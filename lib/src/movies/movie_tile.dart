import 'package:flutter/material.dart';
import 'package:technight_tmdb/src/movies/movie_poster.dart';
import 'package:technight_tmdb/src/movies/movie_screen.dart';
import 'package:technight_tmdb/src/tmdb/tmdb_movie.dart';

class MovieTile extends StatelessWidget {
  final TmdbMovie movie;

  MovieTile(this.movie);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Hero(
          tag: "movieposter/${movie.id}",
          child: MoviePoster(movie.posterPath),
        ),
        Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              final route = MaterialPageRoute(
                builder: (context) {
                  return MovieScreen(movie);
                },
              );
              Navigator.of(context).push(route);
            },
          ),
        ),
      ],
    );
  }
}
