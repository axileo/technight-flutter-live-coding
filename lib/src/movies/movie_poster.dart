import 'package:flutter/material.dart';

const _baseUrl = "https://image.tmdb.org/t/p/w500";

class MoviePoster extends StatelessWidget {
  final String posterPath;

  const MoviePoster(this.posterPath);

  @override
  Widget build(BuildContext context) {
    final imageUrl = "$_baseUrl/$posterPath";
    return Image.network(imageUrl);
  }
}
