import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:technight_tmdb/src/movies/movie_tile.dart';
import 'package:technight_tmdb/src/tmdb/tmdb_movie.dart';
import 'package:technight_tmdb/src/tmdb/tmdb_service.dart';

class MoviesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final tmdb = Provider.of<TmdbService>(context);
    return Scaffold(
      body: FutureBuilder<List<TmdbMovie>>(
        future: tmdb.getMovies(),
        builder: (context, snapshot) {
          if (snapshot.hasError) return _Error();
          if (!snapshot.hasData) return _Loading();

          final movies = snapshot.data;
          return GridView.builder(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 2 / 3,
              crossAxisSpacing: 1,
              mainAxisSpacing: 1,
            ),
            itemCount: movies.length,
            itemBuilder: (context, index) {
              final movie = movies[index];
              return MovieTile(movie);
            },
          );
        },
      ),
    );
  }
}

class _Error extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text("Sorry there was an error."));
  }
}

class _Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: CircularProgressIndicator());
  }
}
