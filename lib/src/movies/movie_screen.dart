import 'package:flutter/material.dart';
import 'package:technight_tmdb/src/movies/movie_header.dart';
import 'package:technight_tmdb/src/tmdb/tmdb_movie.dart';

class MovieScreen extends StatelessWidget {
  final TmdbMovie movie;

  MovieScreen(this.movie);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          MovieHeader(movie),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Text(
                  movie.overview,
                  style: TextStyle(fontSize: 16),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
