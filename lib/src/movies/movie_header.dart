import 'package:flutter/material.dart';
import 'package:technight_tmdb/src/movies/movie_poster.dart';
import 'package:technight_tmdb/src/tmdb/tmdb_movie.dart';

const _baseUrl = "https://image.tmdb.org/t/p/original";

class MovieHeader extends StatelessWidget {
  final TmdbMovie movie;

  MovieHeader(this.movie);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      child: Stack(
        children: [
          Positioned.fill(
            child: Image.network(
              "$_baseUrl/${movie.backdropPath}",
              fit: BoxFit.cover,
            ),
          ),
          Container(color: Colors.black26),
          Positioned(
            left: 8,
            right: 8,
            bottom: 8,
            child: Row(
              children: [
                SizedBox(
                  width: 120,
                  child: Hero(
                    tag: "movieposter/${movie.id}",
                    child: MoviePoster(movie.posterPath),
                  ),
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Text(
                    movie.title,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 26,
                      shadows: [
                        Shadow(blurRadius: 8, color: Colors.black),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
